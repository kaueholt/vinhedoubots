<?php

namespace App\Http\Controllers;

class CustomerController extends Controller
{
    public function getCustomers()
    {
        $url = "http://www.mocky.io/v2/598b16291100004705515ec5";
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89 AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch); // Execute
        if (curl_errno($ch)) {
            $response = curl_error($ch);
        }
        curl_close($ch); // Close
        return json_decode($response, true);
    }
}
