<?php

namespace App\Http\Controllers;

// # 1 - Liste os clientes ordenados pelo  maior valor total em compras. -> getCustomersOrderedBySpentAmount
// # 2 - Mostre o cliente com maior compra única no último ano (2016). -> getBestCustomerByYear
// # 3 - Liste os clientes mais fiéis. -> getMostLoyalCustomers
// # 4 - Recomende um vinho para um determinado cliente a partir do histórico de compras. -> getWineSuggestion

class SalesController extends Controller
{
    private function getAllSales()
    {
        $url = "http://www.mocky.io/v2/598b16861100004905515ec7";
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89 AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch); // Execute
        if (curl_errno($ch)) {
            $response = curl_error($ch);
        }
        curl_close($ch); // Close
        return json_decode($response, true);
    }

    private function getSalesGroupedByCustomer()
    {
        $allSales = $this->getAllSales();
        $allCustomers = new CustomerController();
        $allCustomers = $allCustomers->getCustomers();
        $groupedList = [];
        foreach ($allSales as $venda) {
            foreach ($allCustomers as $cliente) {
                if (
                    ltrim(preg_replace('/[^A-Za-z0-9]/', '', $cliente['cpf']), '0') ==
                    ltrim(preg_replace('/[^A-Za-z0-9]/', '', $venda['cliente']), '0')
                )  //verifica se o regex do cpf da compra bate com algum regex de cpf do cadastro de clientes
                    $venda['clienteInfo'] = $cliente;
            }
            $groupedList[$venda['clienteInfo']['cpf']][] = $venda;
        }
        return $groupedList;
    }

    public function getCustomersOrderedBySpentAmount($deep = false)
    {
        $groupedList = $this->getSalesGroupedByCustomer();
        foreach ($groupedList as $key => $comprasClientes) {
            $total = 0;
            foreach ($comprasClientes as $compra) {
                $total += $compra['valorTotal'];
            }
            $deep ? null : $groupedList[$key] = []; // remove o "lixo estético" do JSON (dados talvez desnecessários)
            $groupedList[$key]['totalGlobal'] = round($total, 2);
        }
        arsort($groupedList);
        return $groupedList;
    }

    public function getBestCustomerByYear($year = '2016', $deep = false)
    {
        $groupedList = $this->getSalesGroupedByCustomer();
        foreach ($groupedList as $key => $comprasClientes) {
            $total = 0;
            foreach ($comprasClientes as $compra) {
                if (isset($compra['data'])) {
                    explode('-', $compra['data'])[2] == $year // verifica se o ano da compra é igual ao ano solicitado 
                        ?   $total += $compra['valorTotal']
                        :   null;
                }
            }
            $deep ? null : $groupedList[$key] = []; // remove o "lixo estético" do JSON (dados talvez desnecessários)
            $groupedList[$key]['total' . $year] = round($total, 2);
        }
        arsort($groupedList);
        return $groupedList;
    }

    public function getMostLoyalCustomers($deep = false)
    {
        $groupedList = $this->getSalesGroupedByCustomer();
        foreach ($groupedList as $key => $comprasClientes) {
            $numeroDeCompras = sizeof($groupedList[$key]);
            $deep ? null : $groupedList[$key] = []; // remove o "lixo estético" do JSON (dados talvez desnecessários)
            $groupedList[$key]['numeroDeCompras'] = $numeroDeCompras;
        }
        arsort($groupedList);
        return $groupedList;
    }

    public function getWineSuggestion($cpf)
    {
        $groupedList = $this->getSalesGroupedByCustomer();
        if (!isset($groupedList[$cpf])) {
            return 'CPF não encontrado.';
        }
        $comprasCliente = $groupedList[$cpf];
        $vinhos = [];
        foreach ($comprasCliente as $compra) {
            foreach ($compra['itens'] as $item) {
                $vinhos[$item['produto'].' - '.$item['variedade']][] = $item;
            }
        }
        arsort($vinhos);
        return array_keys(array_slice($vinhos,0,1));
    }
}
