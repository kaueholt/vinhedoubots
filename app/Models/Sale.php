<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    public function __construct($sale)
    {
        $this->items = $sale['itens'];
        $this->costumer = $sale['cliente'];
    }
    
    protected $fillable = [
        'codigo',
        'data',
        'cliente',
        'itens',
        'valorTotal'
    ];

    protected $dates = [
        'data:d-m-Y'
    ];

    protected $casts = [
        'valorTotal' => 'float'
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Saleitem');
    }
    public function costumer()
    {
        return $this->belongsTo('App\Models\Costumer');
    }
}
