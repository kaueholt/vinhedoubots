<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'cpf'
    ];

    protected $casts = [
        'id' => 'int'
    ];

    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }
}
