<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    protected $fillable = [
        'produto',
        'variedade',
        'pais',
        'categoria',
        'safra',
        'preco'
    ];

    protected $casts = [
        'preco' => 'float'
    ];

    public function sale()
    {
        return $this->belongsTo('App\Models\Sale');
    }
}
