<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CostumerController;
use App\Http\Controllers\SalesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::get('/', function () {
        return response()->json(['message' => 'Vinhedo API', 'status' => 'Connected']);
    });
    Route::get('/clientes', 'CostumerController@getCustomers');
    Route::get('/questao/1', 'SalesController@getCustomersOrderedBySpentAmount');
    Route::get('/questao/1/{deep}', 'SalesController@getCustomersOrderedBySpentAmount');
    Route::get('/questao/2', 'SalesController@getBestCustomerByYear');
    Route::get('/questao/2/{year}', 'SalesController@getBestCustomerByYear');
    Route::get('/questao/3', 'SalesController@getMostLoyalCustomers');
    Route::get('/questao/3/{deep}', 'SalesController@getMostLoyalCustomers');
    Route::get('/questao/4/{cpf}', 'SalesController@getWineSuggestion');
});

Route::get('/', function () {
    return redirect('api');
});
